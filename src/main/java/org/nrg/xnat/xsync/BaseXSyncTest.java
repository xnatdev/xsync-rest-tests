package org.nrg.xnat.xsync;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.jayway.restassured.path.json.JsonPath;
import org.apache.commons.lang3.time.StopWatch;
import org.nrg.testing.CommonUtils;
import org.nrg.testing.util.RandomHelper;
import org.nrg.testing.xnat.BaseRestTest;
import org.nrg.testing.xnat.conf.Settings;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.xnat.conf.XnatConfig;
import org.nrg.testing.xnat.rest.XnatRestDriver;
import org.nrg.xnat.interfaces.XnatInterface;
import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.Experiment;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.SubjectAssessor;
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.pogo.users.User;
import org.nrg.xnat.rest.XnatAliasToken;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.nrg.xnat.xsync.enums.OverallStatus;
import org.nrg.xnat.xsync.enums.SyncStatus;
import org.nrg.xnat.xsync.jackson.modules.XSyncDeserializationModule;
import org.nrg.xnat.xsync.jackson.modules.XSyncSerializationModule;
import org.nrg.xnat.xsync.results.ExperimentHistory;
import org.nrg.xnat.xsync.results.SubjectHistory;
import org.nrg.xnat.xsync.results.XSyncHistory;
import org.nrg.xnat.xsync.results.XSyncResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class BaseXSyncTest extends BaseRestTest {

    protected static XnatConfig sourceXnat;
    protected static XnatConfig destinationXnat;
    protected static XnatRestDriver sourceXnatClient;
    protected static XnatRestDriver destinationXnatClient;
    protected static User sourceUser;
    protected static User destinationUser;
    protected static Project sourceProject;
    protected static Project destinationProject;
    protected static final int DEFAULT_WAIT = 600;
    protected static final long DEFAULT_RESOURCE_SIZE_TOLERANCE = 20000;
    protected static final String DICOM = "DICOM";
    protected static final String NIFTI = "NIFTI";

    public void configureJackson() {
        XnatRestDriver.XNAT_REST_MAPPER.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        XnatRestDriver.XNAT_REST_MAPPER.registerModule(XSyncSerializationModule.build());
        XnatRestDriver.XNAT_REST_MAPPER.registerModule(XSyncDeserializationModule.build());
    }

    public void setupXnats() {
        if (Settings.OTHER_XNAT_CONFIGS.isEmpty()) {
            throw new RuntimeException("XSync tests require a destination server (which could be the same as the source server). Specify the destination server properties using the xnat2 prefix instead of xnat (for example xnat2.main.baseurl). Also include xnat2.required=true.");
        }

        sourceXnat = Settings.DEFAULT_XNAT_CONFIG;
        destinationXnat = Settings.OTHER_XNAT_CONFIGS.get(0);
        LOGGER.info(String.format("Source and destination XSync configs found: source = %s, destination = %s.", sourceXnat.getXnatUrl(), destinationXnat.getXnatUrl()));

        // source init and setup should already be handled by nrg_selenium, so init destination XNAT if needed

        if (destinationXnat.getInitSetting()) {
            final XnatRestDriver destinationDriver = XnatRestDriver.getInstance(destinationXnat);
            destinationDriver.initializeXnat();
            destinationDriver.setupTestUsers();
        } else {
            XnatInterface.authenticate(destinationXnat.getXnatUrl(), destinationXnat.getMainUser(), true);
        }

        sourceXnatClient = restDriver;
        destinationXnatClient = XnatRestDriver.getInstance(destinationXnat);
        destinationXnatClient.setTestController(testController);
        sourceUser = sourceXnat.getMainUser();
        destinationUser = destinationXnat.getMainUser();
    }

    @BeforeSuite
    public void setupXSyncTests() {
        configureJackson();
        setupXnats();
    }

    @BeforeClass()
    public void randomizeClassProjects() {
        sourceProject = new Project("XSYNC_" + RandomHelper.randomID(6));
        destinationProject = new Project("XSYNC_" + RandomHelper.randomID(6));
    }

    protected void baseXSyncSetup(XSyncConfig config) {
        config.destinationXnat(destinationXnat.getXnatUrl()).destinationProject(destinationProject).sourceProject(sourceProject);

        sourceXnatClient.createProject(sourceUser, sourceProject);
        destinationXnatClient.createProject(destinationXnat.getMainUser(), destinationProject);
        setupXSyncConfig(sourceUser, config, destinationUser);
    }

    protected void uploadAndLogSessions(User authUser, Project project, List<? extends ImagingSession> sessions) {
        uploadAndLogSessions(restDriver, authUser, project, sessions);
    }

    protected void uploadAndLogSessions(XnatRestDriver driver, User authUser, Project project, List<? extends ImagingSession> sessions) {
        LOGGER.info("Uploading sessions through the session zip importer...");
        for (ImagingSession session : sessions) {
            session.project(project);
            if (session.getExtension() == null) {
                session.extension(new SessionImportExtension(restDriver.interfaceFor(authUser), session, getXSyncData(session)));
            }
            driver.createSubjectAssessor(authUser, session);
            driver.waitForAutoRun(session);
            LOGGER.info(String.format("Session %s uploaded to session zip importer...", session.getLabel()));
        }
    }

    protected File getXSyncData(ImagingSession session) {
        return getXSyncData(session.getLabel());
    }

    protected File getXSyncData(String session) {
        return Paths.get(Settings.DATA_LOCATION, TestData.XSYNC_DATA.getName(), TestData.XSYNC_DATA.getName(), session + ".zip").toFile(); // duplication of TestData.XSYNC_DATA.getName() is not a typo
    }

    protected void setupXSyncConfig(User authUser, XSyncConfig xSyncConfig, User destinationUser) {
        final Object postBody = (xSyncConfig.getExtension() != null) ? ((XSyncConfigExtension)xSyncConfig.getExtension()).postBody() : xSyncConfig;

        restDriver.interfaceFor(authUser).queryBase().body(postBody).post(restDriver.formatXapiUrl("/xsync/setup/projects/", xSyncConfig.getSourceProject().getId())).then().assertThat().statusCode(200);
        setXSyncCredentials(authUser, xSyncConfig, destinationUser);
        setXSyncAnonScript(authUser, xSyncConfig);
    }

    protected void setXSyncCredentials(User authUser, XSyncConfig xSyncConfig, User destinationUser) {
        final Map<String, String> remoteRestBody = new HashMap<>();
        remoteRestBody.put("url", CommonUtils.formatUrl(xSyncConfig.getDestinationXnat(), "/data/services/tokens/issue"));
        remoteRestBody.put("method", "GET");
        remoteRestBody.put("username", destinationUser.getUsername());
        remoteRestBody.put("password", destinationUser.getPassword());
        final JsonPath response = restDriver.interfaceFor(authUser).queryBase().contentType(ContentType.JSON).body(remoteRestBody).post(restDriver.formatXapiUrl("/xsync/remoteREST")).then().assertThat().statusCode(200).and().extract().response().jsonPath();
        final XnatAliasToken remoteToken = response.getObject("", XnatAliasToken.class);

        final Map<String, Object> credentialsBody = new HashMap<>();
        credentialsBody.put("host", xSyncConfig.getDestinationXnat());
        credentialsBody.put("localProject", xSyncConfig.getSourceProject().getId());
        credentialsBody.put("remoteProject", xSyncConfig.getDestinationProject().getId());
        credentialsBody.put("syncNewOnly", xSyncConfig.isSyncNewOnly());
        credentialsBody.put("alias", remoteToken.getAlias());
        credentialsBody.put("secret", remoteToken.getSecret());
        credentialsBody.put("username", destinationUser.getUsername());
        credentialsBody.put("estimatedExpirationTime", response.getLong("estimatedExpirationTime"));

        restDriver.interfaceFor(authUser).queryBase().contentType(ContentType.TEXT).body(credentialsBody, ObjectMapperType.JACKSON_2).post(restDriver.formatXapiUrl("xsync/credentials/save/projects/", xSyncConfig.getDestinationProject().getId())).then().assertThat().statusCode(200);
    }

    protected void setXSyncAnonScript(User authUser, XSyncConfig xSyncConfig) {
        if (xSyncConfig.getAnonScript() != null) {
            restDriver.interfaceFor(authUser).queryBase().body(xSyncConfig.getAnonScript().getContents()).
                    put(restDriver.formatXapiUrl("/xsync/setup/presyncanonymization/projects", xSyncConfig.getSourceProject().getId())).
                    then().assertThat().statusCode(200);
        }
    }

    protected void launchProjectSync(User authUser, Project project) {
        final int previousHistoryId = getLastSyncResult(authUser, project).getHistoryId();
        restDriver.interfaceFor(authUser).queryBase().post(restDriver.formatXapiUrl("xsync/projects/", project.getId())).then().assertThat().statusCode(200);
        waitForSyncRegistered(authUser, project, previousHistoryId);
    }

    protected void manualSync(User authUser, Experiment experiment) {
        final int previousHistoryId = getLastSyncResult(authUser, experiment.getPrimaryProject()).getHistoryId();
        restDriver.interfaceFor(authUser).queryBase().post(restDriver.formatXapiUrl("/xsync/syncexperiment/", experiment.getAccessionNumber())).then().assertThat().statusCode(200);
        waitForSyncRegistered(authUser, experiment.getPrimaryProject(), previousHistoryId);
    }

    protected XSyncResult getLastSyncResult(User authUser, Project project) {
        return restDriver.interfaceFor(authUser).queryBase().get(restDriver.formatXapiUrl("xsync/syncStatus/projects", project.getId())).then().assertThat().statusCode(200).and().extract().as(XSyncResult.class);
    }

    protected void waitForSyncRegistered(User authUser, Project project, int previousId) {
        final StopWatch stopWatch = CommonUtils.launchStopWatch();
        final int max = 30;

        while(true) {
            CommonUtils.checkStopWatch(stopWatch, max, "Sync did not begin within maximum number of seconds: " + max);

            final XSyncResult result = getLastSyncResult(authUser, project);
            if (result.getHistoryId() > previousId || result.isSyncing()) return;
            CommonUtils.sleep(100);
        }
    }

    protected XSyncResult waitForSyncComplete(User authUser, int maximumTime, Project project) {
        final StopWatch stopWatch = CommonUtils.launchStopWatch();

        while(true) {
            CommonUtils.checkStopWatch(stopWatch, maximumTime, "Sync did not complete within maximum number of seconds: " + maximumTime);

            final XSyncResult result = getLastSyncResult(authUser, project);
            if (!result.isSyncing() && result.getHistoryId() > 0) return result;
            CommonUtils.sleep(100);
        }
    }

    protected XSyncHistory readSyncHistory(User authUser, Project project, XSyncResult result) {
        return readSyncHistory(authUser, project, result.getHistoryId());
    }

    protected XSyncHistory readSyncHistory(User authUser, Project project, int historyId) {
        return restDriver.interfaceFor(authUser).queryBase().get(restDriver.formatXapiUrl("xsync/history/projects", project.getId(), String.valueOf(historyId))).then().assertThat().statusCode(200).and().extract().as(XSyncHistory.class);
    }

    protected void assertSubjectsSyncedAndVerified(XSyncHistory history, List<Subject> subjects) {
        final List<String> expectedSubjectLabels = new ArrayList<>();
        final List<String> actualSubjectLabels = new ArrayList<>();

        for (Subject subject : subjects) {
            expectedSubjectLabels.add(subject.getLabel());
        }

        for (SubjectHistory subjectHistoryEntry : history.getSubjectHistories()) {
            assertEquals(SyncStatus.SYNCED_AND_VERIFIED, subjectHistoryEntry.getSyncStatus());
            actualSubjectLabels.add(subjectHistoryEntry.getLocalLabel());
        }

        Collections.sort(expectedSubjectLabels);
        Collections.sort(actualSubjectLabels);
        assertEquals(expectedSubjectLabels, actualSubjectLabels);
    }

    protected void assertSubjectAssessorsSyncedAndVerified(XSyncHistory history, List<? extends SubjectAssessor> subjectAssessors) {
        final List<String> expectedLabels = new ArrayList<>();
        final List<String> actualLabels = new ArrayList<>();

        for (SubjectAssessor assessor : subjectAssessors) {
            expectedLabels.add(assessor.getLabel());
        }

        for (ExperimentHistory experimentHistoryEntry : history.getExperimentHistories()) {
            assertEquals(SyncStatus.SYNCED_AND_VERIFIED, experimentHistoryEntry.getSyncStatus());
            actualLabels.add(experimentHistoryEntry.getLocalLabel());
        }

        Collections.sort(expectedLabels);
        Collections.sort(actualLabels);
        assertEquals(expectedLabels, actualLabels);
    }

    protected XSyncHistory assertAllSyncedAndVerified(User authUser, int maxWait, Project project, List<Subject> subjects, List<? extends SubjectAssessor> subjectAssessors) {
        final XSyncResult result = waitForSyncComplete(authUser, maxWait, project);
        assertEquals(subjects.size(), result.getCompletedSubjects().size());
        assertEquals(subjectAssessors.size(), result.getCompletedExperiments().size());

        final XSyncHistory history = readSyncHistory(authUser, project, result);
        assertEquals(OverallStatus.COMPLETE_AND_VERIFIED, history.getSyncStatus());
        assertSubjectsSyncedAndVerified(history, subjects);
        assertSubjectAssessorsSyncedAndVerified(history, subjectAssessors);
        return history;
    }

    protected SubjectHistory historyFor(XSyncHistory history, Subject subject) {
        for (SubjectHistory subjectHistory : history.getSubjectHistories()) {
            if (subjectHistory.getLocalLabel().equals(subject.getLabel())) return subjectHistory;
        }
        return null;
    }

    protected ExperimentHistory historyFor(XSyncHistory history, Experiment experiment) {
        for (ExperimentHistory experimentHistory : history.getExperimentHistories()) {
            if (experimentHistory.getLocalLabel().equals(experiment.getLabel())) return experimentHistory;
        }
        return null;
    }

    protected void addSessionNotes(User authUser, String note, ImagingSession session) {
        addSessionNotes(sourceXnatClient, authUser, note, session);
    }

    protected void addSessionNotes(XnatRestDriver driver, User authUser, String note, ImagingSession session) {
        driver.interfaceFor(authUser).queryBase().queryParam("note", note).put(driver.subjectAssessorUrl(session)).then().assertThat().statusCode(200);
        session.notes(note);
    }

    protected Project readDestinationProject(User authUser, Project project) {
        return destinationXnatClient.readProject(authUser, project.getId());
    }

    protected ImagingSession findDestinationSession(User authUser, Project project, String subjectLabel, String subjectAssessorLabel) {
        final Subject subject = destinationXnatClient.findSubject(readDestinationProject(authUser, project), subjectLabel);
        return destinationXnatClient.findSubjectAssessor(subject, subjectAssessorLabel).subject(subject).project(project);
    }

    protected ImagingSession findDestinationSessionShortcut(ImagingSession sourceSession) {
        return findDestinationSession(destinationUser, destinationProject, sourceSession.getSubject().getLabel(), sourceSession.getLabel());
    }

    protected String markExperimentSyncStatus(User authUser, Experiment experiment, boolean status) {
        return restDriver.interfaceFor(authUser).queryBase().queryParam("okToSync", status).post(restDriver.formatXapiUrl("/xsync/experiments", experiment.getAccessionNumber())).
                then().assertThat().statusCode(200).and().extract().response().asString();
    }

    protected String markExperimentOkToSync(User authUser, Experiment experiment) {
        final String response = markExperimentSyncStatus(authUser, experiment, true);
        assertTrue(response.contains(experiment.getAccessionNumber() + " has been marked OK to sync"));
        return response;
    }

    protected String markExperimentNotOkToSync(User authUser, Experiment experiment) {
        final String response = markExperimentSyncStatus(authUser, experiment, false);
        assertTrue(response.contains(experiment.getAccessionNumber() + " has been marked Not OK to sync"));
        return response;
    }

    protected void simpleResourceCheck(Resource resource, int expectedNumFiles, long expectedSumFileSize, long comparisonTolerance) {
        assertEquals(expectedNumFiles, resource.getFileCount());
        assertEquals(expectedSumFileSize, resource.getFileSize(), comparisonTolerance);
    }

    protected void simpleResourceCheck(Resource resource, int expectedNumFiles, long expectedSumFileSize) {
        simpleResourceCheck(resource, expectedNumFiles, expectedSumFileSize, DEFAULT_RESOURCE_SIZE_TOLERANCE);
    }

    protected void simpleResourceCheck(Resource expectedResource, Resource actualResource, long comparisonTolerance) {
        simpleResourceCheck(actualResource, expectedResource.getFileCount(), expectedResource.getFileSize(), comparisonTolerance);
    }

    protected void simpleResourceCheck(Resource expectedResource, Resource actualResource) {
        simpleResourceCheck(expectedResource, actualResource, DEFAULT_RESOURCE_SIZE_TOLERANCE);
    }

    protected Resource dicomFrom(List<Resource> resources) {
        return restDriver.findResource(resources, DICOM); // which XnatRestDriver object doesn't actually matter here (the method in XnatInterface could be static)
    }

    protected Resource niftiFrom(List<Resource> resources) {
        return restDriver.findResource(resources, NIFTI); // which XnatRestDriver object doesn't actually matter here (the method in XnatInterface could be static)
    }

}
