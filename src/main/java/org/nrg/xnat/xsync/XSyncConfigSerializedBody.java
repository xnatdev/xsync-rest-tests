package org.nrg.xnat.xsync;

import org.nrg.xnat.xsync.config.XSyncConfig;

public class XSyncConfigSerializedBody extends XSyncConfigExtension {

    public XSyncConfigSerializedBody(XSyncConfig config) {
        setParentObject(config);
    }

    @Override
    public Object postBody() {
        return getParentObject();
    }

}
