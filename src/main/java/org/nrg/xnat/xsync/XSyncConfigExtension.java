package org.nrg.xnat.xsync;

import org.nrg.xnat.pogo.Extension;
import org.nrg.xnat.xsync.config.XSyncConfig;

public abstract class XSyncConfigExtension extends Extension<XSyncConfig> {

    public abstract Object postBody();

}
