package org.nrg.xnat.xsync;

import org.nrg.xnat.pogo.Project;
import org.nrg.xnat.util.FileIOUtils;
import org.nrg.xnat.xsync.config.XSyncConfig;

import java.io.File;

public class XSyncConfigFileExtension extends XSyncConfigExtension {

    private String contents;

    public XSyncConfigFileExtension(XSyncConfig config, File file, Project sourceProject, String destinationXnat, Project destinationProject) {
        setParentObject(config);
        contents = FileIOUtils.readFile(file).replace("$SOURCE_PROJECT", sourceProject.getId()).replace("$DESTINATION_URL", destinationXnat).replace("$DESTINATION_PROJECT", destinationProject.getId());
    }

    @Override
    public Object postBody() {
        return contents;
    }

}
