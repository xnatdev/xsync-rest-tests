package org.nrg.xnat.xsync.tests;

import org.apache.commons.lang3.StringUtils;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.xsync.BaseXSyncTest;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.nrg.xnat.xsync.enums.OverallStatus;
import org.nrg.xnat.xsync.enums.SyncProcedureType;
import org.nrg.xnat.xsync.enums.SyncStatus;
import org.nrg.xnat.xsync.results.XSyncHistory;
import org.nrg.xnat.xsync.results.XSyncResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

@SuppressWarnings("FieldCanBeLocal")
@TestRequires(data = TestData.XSYNC_DATA)
public class XSyncServerSwitchTests extends BaseXSyncTest {

    private final XSyncConfig config = new XSyncConfig();
    private Subject subject1;
    private Subject subject2;
    private Subject subject3;
    private ImagingSession subject1_mr1;
    private ImagingSession subject1_mr2;
    private ImagingSession subject2_mr1;
    private ImagingSession subject3_mr1;

    @BeforeClass
    public void setupConfig() {
        baseXSyncSetup(config);
    }

    @Test
    public void testServerSwitch() throws IOException {
        final String destinationWithPort = destinationUrlWithPort();
        final String sessionNotes = "Testing note for XSync.";
        final String scanNotes = "Scan note for XSync test.";
        final String quality = "questionable";

        subject1 = new Subject(sourceProject, "SPP_0x960c08");
        subject2 = new Subject(sourceProject, "SPP_0x5533ff");
        subject3 = new Subject(sourceProject, "SPP_0xff2fb3");
        subject1_mr1 = new MRSession(sourceProject, subject1, "SPP_0x960c08_MR1");
        subject1_mr2 = new MRSession(sourceProject, subject1, "SPP_0x960c08_MR2");
        subject2_mr1 = new MRSession(sourceProject, subject2, "SPP_0x5533ff_MR1");
        subject3_mr1 = new MRSession(sourceProject, subject3, "SPP_0xff2fb3_MRI");

        final List<Subject> firstSyncSubjects = Arrays.asList(subject1, subject2);
        final List<ImagingSession> firstSyncSessions = Arrays.asList(subject1_mr1, subject2_mr1);

        uploadAndLogSessions(sourceUser, sourceProject, firstSyncSessions);
        launchProjectSync(sourceUser, sourceProject);
        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, firstSyncSubjects, firstSyncSessions);

        final ImagingSession initialDestinationSession1 = findDestinationSessionShortcut(subject1_mr1);
        final ImagingSession initialDestinationSession2 = findDestinationSessionShortcut(subject2_mr1);
        final Scan initialDestinationSession2Scan = initialDestinationSession2.getScans().get(0);

        addSessionNotes(destinationXnatClient, destinationUser, sessionNotes, initialDestinationSession1);
        destinationXnatClient.updateScan(destinationUser, initialDestinationSession2Scan.quality(quality).note(scanNotes));

        setupXSyncConfig(sourceUser, config.destinationXnat(destinationWithPort), destinationUser);

        final List<ImagingSession> secondSyncSessions = Arrays.asList(subject1_mr2, subject3_mr1);

        uploadAndLogSessions(sourceUser, sourceProject, secondSyncSessions);
        launchProjectSync(sourceUser, sourceProject);

        final XSyncResult finalSyncResult = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(SyncProcedureType.PROJECT_SYNC, finalSyncResult.getSyncType());
        assertEquals(3, finalSyncResult.getCompletedSubjects().size());
        assertEquals(2, finalSyncResult.getCompletedExperiments().size());

        final XSyncHistory finalHistory = readSyncHistory(sourceUser, sourceProject, finalSyncResult);
        assertEquals(OverallStatus.COMPLETE, finalHistory.getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(finalHistory, subject3).getSyncStatus());
        assertEquals(SyncStatus.SKIPPED, historyFor(finalHistory, subject1_mr1).getSyncStatus());
        assertEquals(SyncStatus.SKIPPED, historyFor(finalHistory, subject2_mr1).getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(finalHistory, subject1_mr2).getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(finalHistory, subject3_mr1).getSyncStatus());

        final ImagingSession finalDestinationSession1 = findDestinationSessionShortcut(subject1_mr1);
        final ImagingSession finalDestinationSession2 = findDestinationSessionShortcut(subject2_mr1);
        final Scan finalDestinationSession2Scan = finalDestinationSession2.getScans().get(0);
        assertEquals(sessionNotes, finalDestinationSession1.getNotes());
        assertEquals(scanNotes, finalDestinationSession2Scan.getNote());
        assertEquals(quality, finalDestinationSession2Scan.getQuality());
    }

    private String destinationUrlWithPort(int port) {
        return StringUtils.stripEnd(destinationXnat.getXnatUrl(), "/") + ":" + port;
    }

    private boolean portValid(int port) {
        try {
            return destinationXnatClient.mainCredentials().get(destinationUrlWithPort(port)).statusCode() == 200;
        } catch (Throwable ignored) {
            return false;
        }
    }

    private String destinationUrlWithPort() {
        if (portValid(80)) {
            return destinationUrlWithPort(80);
        }
        if (portValid(443)) {
            return destinationUrlWithPort(443);
        }
        if (portValid(8080)) {
            return destinationUrlWithPort(8080);
        }
        throw new RuntimeException("Couldn't find a valid port to specify on the destination server");
    }
}
