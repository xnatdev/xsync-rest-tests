package org.nrg.xnat.xsync.tests;

import org.nrg.testing.CommonUtils;
import org.nrg.testing.annotations.HardDependency;
import org.nrg.testing.annotations.SoftDependency;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.CTSession;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.xsync.BaseXSyncTest;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.nrg.xnat.xsync.enums.OverallStatus;
import org.nrg.xnat.xsync.enums.SyncProcedureType;
import org.nrg.xnat.xsync.enums.SyncStatus;
import org.nrg.xnat.xsync.results.XSyncHistory;
import org.nrg.xnat.xsync.results.XSyncResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@SuppressWarnings("FieldCanBeLocal")
@TestRequires(data = TestData.XSYNC_DATA)
public class BasicXSyncConfigTests extends BaseXSyncTest {

    private final XSyncConfig basicConfig = new XSyncConfig();

    private Subject subject1;
    private Subject subject2;
    private Subject subject3;
    private Subject subject4;
    private Subject subject5;

    private MRSession subj1_mr1;
    private MRSession subj2_mr1;
    private CTSession subj1_ct1;
    private MRSession subj2_mr2;
    private MRSession subj3_mr1;
    private MRSession subj3_mr2;
    private MRSession subj3_mr3;
    private MRSession subj3_mr3_dest;
    private MRSession subj4_mr1;
    private MRSession subj4_mr2;
    private MRSession subj5_mr1;
    private MRSession subj5_mr2;

    @BeforeClass
    public void setupConfig() {
        baseXSyncSetup(basicConfig);
    }

    @Test
    public void testBasicSync() {
        subject1 = new Subject().label("SPP_0xff2fb3");
        subject2 = new Subject().label("SPP_0x5533ff");
        final List<Subject> subjects = Arrays.asList(subject1, subject2);

        subj1_mr1 = new MRSession("SPP_0xff2fb3_MRI").subject(subject1);
        subj2_mr1 = new MRSession("SPP_0x5533ff_MR1").subject(subject2);
        subj1_ct1 = new CTSession("SPP_0xff2fb3_CT").subject(subject1);
        final List<ImagingSession> sessions = Arrays.asList(subj1_mr1, subj2_mr1, subj1_ct1);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);
        launchProjectSync(sourceUser, sourceProject);
        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, subjects, sessions);
    }

    @Test
    @HardDependency("testBasicSync")
    public void testBasicIgnoredSourceChanges() {
        final String scanNote = "XSync scan note.";
        final String sessionNote = "XSync session note.";
        final String originalScanQuality = "usable";
        final String scanQuality = "questionable";

        subject3 = new Subject().label("C0");
        final List<Subject> subjects = Arrays.asList(subject1, subject2, subject3);

        subj2_mr2 = new MRSession("SPP_0x5533ff_MR2").subject(subject2);
        subj3_mr1 = new MRSession("C01448_MR").subject(subject3); // does not match DICOM
        final List<MRSession> sessions = Arrays.asList(subj2_mr2, subj3_mr1);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        subj1_mr1.notes(sessionNote);
        addSessionNotes(sourceUser, sessionNote, subj1_mr1);

        final Scan scan = restDriver.readScans(sourceUser, sourceProject, subject1, subj1_mr1).get(0).note(scanNote).quality(scanQuality);
        restDriver.updateScan(sourceUser, scan); // add scan notes & update scan quality

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, subjects, sessions);

        final ImagingSession destinationSession = findDestinationSessionShortcut(subj1_mr1);

        assertEquals(null, destinationSession.getNotes());

        final Scan destinationScan = destinationXnatClient.findScan(destinationSession, scan.getId());
        assertEquals(null, destinationScan.getNote());
        assertEquals(originalScanQuality, destinationScan.getQuality());
    }

    @Test
    @HardDependency("testBasicIgnoredSourceChanges")
    public void testBasicSyncConflictSkip() {
        final String sessionNote = "XSync session note.";
        final int numDicomFiles = 112;
        final long dicomFileSize = 50006708;

        final List<Subject> subjects = Collections.singletonList(subject3);

        subj3_mr2 = new MRSession("C01456_MR").subject(subject3); // does not match DICOM
        subj3_mr3 = new MRSession("C01459_MR").subject(subject3); // does not match DICOM
        final List<MRSession> sessions = Arrays.asList(subj3_mr2, subj3_mr3);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        subj3_mr3_dest = new MRSession(subj3_mr3.getLabel()).subject(subject3); // does not match DICOM
        subj3_mr3_dest.extension(new SessionImportExtension(destinationXnatClient.interfaceFor(destinationUser), subj3_mr3_dest, getXSyncData("W32")));

        uploadAndLogSessions(destinationXnatClient, destinationUser, destinationProject, Collections.singletonList(subj3_mr3_dest));
        addSessionNotes(destinationXnatClient, destinationUser, sessionNote, subj3_mr3_dest);
        final Scan originalScan = destinationXnatClient.readScans(destinationUser, destinationProject, subject3, subj3_mr3_dest).get(0);

        launchProjectSync(sourceUser, sourceProject);

        final XSyncResult result = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(subjects.size(), result.getCompletedSubjects().size());
        assertEquals(sessions.size() - 1, result.getCompletedExperiments().size());

        final XSyncHistory history = readSyncHistory(sourceUser, sourceProject, result);
        assertEquals(OverallStatus.COMPLETE, history.getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(history, subj3_mr2).getSyncStatus());
        assertEquals(SyncStatus.SKIPPED, historyFor(history, subj3_mr3).getSyncStatus());
        assertEquals(sessions.size(), history.getExperimentHistories().size());

        final ImagingSession sessionAfterSync = findDestinationSessionShortcut(subj3_mr3);
        assertEquals(sessionNote, sessionAfterSync.getNotes());

        final Scan scanAfterSync = sessionAfterSync.getScans().get(0);
        assertEquals(originalScan, scanAfterSync);
        assertEquals(originalScan.getType(), scanAfterSync.getType());
        assertEquals(originalScan.getSeriesDescription(), scanAfterSync.getSeriesDescription());

        final Resource dicomAfterSync = restDriver.findResource(scanAfterSync.getScanResources(), "DICOM");
        simpleResourceCheck(dicomAfterSync, numDicomFiles, dicomFileSize);
    }

    @Test
    @HardDependency("testBasicSyncConflictSkip")
    public void testBasicManualSessionSyncOverwrite() {
        final int numDicomFiles = 192;
        final long dicomFileSize = 113836608;
        final String scanId = "13";
        final String seriesDescription = "t1_mprage_sag_novo";

        manualSync(sourceUser, subj3_mr3);

        final XSyncResult result = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(SyncProcedureType.EXPERIMENT_SYNC, result.getSyncType());
        assertEquals(1, result.getCompletedSubjects().size());
        assertEquals(1, result.getCompletedExperiments().size());

        final XSyncHistory history = readSyncHistory(sourceUser, sourceProject, result);
        assertEquals(OverallStatus.COMPLETE_AND_VERIFIED, history.getSyncStatus());
        assertEquals(1, history.getExperimentHistories().size());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(history, subj3_mr3).getSyncStatus());

        final ImagingSession sessionAfterSync = findDestinationSessionShortcut(subj3_mr3);
        assertEquals(null, sessionAfterSync.getNotes());

        final Scan scanAfterSync = sessionAfterSync.getScans().get(0);
        assertEquals(scanId, scanAfterSync.getId());
        assertEquals(seriesDescription, scanAfterSync.getSeriesDescription());

        final Resource dicomAfterSync = restDriver.findResource(scanAfterSync.getScanResources(), "DICOM");
        simpleResourceCheck(dicomAfterSync, numDicomFiles, dicomFileSize);
    }

    @Test
    @HardDependency("testBasicManualSessionSyncOverwrite")
    public void testBasicIgnoreChangesAfterManualSync() {
        final String addedSessionNotes = "Notes for XSync test.";

        subject4 = new Subject().label("SPP_0x220790");
        subj4_mr1 = new MRSession("SPP_0x220790_MR1").subject(subject4);
        subj4_mr2 = new MRSession("SPP_0x220790_MR2").subject(subject4);
        final List<MRSession> sessions = Arrays.asList(subj4_mr1, subj4_mr2);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        manualSync(sourceUser, subj4_mr1);

        final XSyncResult manualSyncResult = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(SyncProcedureType.EXPERIMENT_SYNC, manualSyncResult.getSyncType());
        assertEquals(1, manualSyncResult.getCompletedSubjects().size());
        assertEquals(1, manualSyncResult.getCompletedExperiments().size());

        final XSyncHistory manualSyncHistory = readSyncHistory(sourceUser, sourceProject, manualSyncResult);
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, manualSyncHistory.getExperimentHistories().get(0).getSyncStatus());
        assertEquals(1, manualSyncHistory.getExperimentHistories().size());

        final ImagingSession destinationSession = findDestinationSessionShortcut(subj4_mr1);
        addSessionNotes(destinationXnatClient, destinationUser, addedSessionNotes, destinationSession);

        launchProjectSync(mainUser, sourceProject);

        final XSyncResult projectSyncResult = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(SyncProcedureType.PROJECT_SYNC, projectSyncResult.getSyncType());
        assertEquals(1, projectSyncResult.getCompletedSubjects().size());
        assertEquals(1, projectSyncResult.getCompletedExperiments().size());

        final XSyncHistory projectSyncHistory = readSyncHistory(sourceUser, sourceProject, projectSyncResult);
        assertEquals(OverallStatus.COMPLETE, projectSyncHistory.getSyncStatus());
        assertEquals(SyncStatus.SKIPPED, historyFor(projectSyncHistory, subj4_mr1).getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(projectSyncHistory, subj4_mr2).getSyncStatus());

        assertEquals(addedSessionNotes, findDestinationSessionShortcut(subj4_mr1).getNotes());
    }

    @Test
    @SoftDependency("testBasicIgnoreChangesAfterManualSync")
    public void testBasicConcurrentSyncsBlocked() {
        subject5 = new Subject().label("SPP_0x960c08");
        subj5_mr1 = new MRSession("SPP_0x960c08_MR1").subject(subject5);
        subj5_mr2 = new MRSession("SPP_0x960c08_MR2").subject(subject5);

        List<MRSession> sessions = Arrays.asList(subj5_mr1, subj5_mr2);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        launchProjectSync(sourceUser, sourceProject);

        final XSyncResult result = getLastSyncResult(sourceUser, sourceProject);
        assertTrue(result.isSyncing());

        restDriver.interfaceFor(sourceUser).queryBase().post(restDriver.formatXapiUrl("xsync/projects/", sourceProject.getId())).then().assertThat().statusCode(409);
    }

}
