package org.nrg.xnat.xsync.tests;

import org.nrg.testing.annotations.HardDependency;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.file.FileIO;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.xsync.BaseXSyncTest;
import org.nrg.xnat.xsync.XSyncConfigFileExtension;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.nrg.xnat.xsync.enums.SyncStatus;
import org.nrg.xnat.xsync.results.XSyncHistory;
import org.nrg.xnat.xsync.results.XSyncResult;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@TestRequires(data = TestData.XSYNC_DATA)
public class OkToSyncTests extends BaseXSyncTest {

    private final XSyncConfig okConfig = new XSyncConfig();
    private Subject subject1;
    private ImagingSession subject1_mr1;
    private ImagingSession subject1_mr2;

    @BeforeClass
    public void setupConfig() {
        new XSyncConfigFileExtension(okConfig, FileIO.getDataFile("xsync_ok_to_sync_config.json"), sourceProject, destinationXnat.getXnatUrl(), destinationProject);
        baseXSyncSetup(okConfig);
    }

    @Test
    public void testOkToSyncSkips() {
        subject1 = new Subject().label("SPP_0x960c08");
        subject1_mr1 = new MRSession("SPP_0x960c08_MR1").subject(subject1);
        subject1_mr2 = new MRSession("SPP_0x960c08_MR2").subject(subject1);
        uploadAndLogSessions(sourceUser, sourceProject, Arrays.asList(subject1_mr1, subject1_mr2));

        launchProjectSync(sourceUser, sourceProject);

        final XSyncResult result = waitForSyncComplete(mainUser, DEFAULT_WAIT/10, sourceProject);
        assertEquals(1, result.getCompletedSubjects().size());
        assertEquals(0, result.getCompletedExperiments().size());

        final XSyncHistory history = readSyncHistory(mainUser, sourceProject, result);
        assertEquals(0, history.getExperimentHistories().size());
    }

    @Test
    @HardDependency("testOkToSyncSkips")
    public void testOkToSyncSuccessfulSync() {
        markExperimentOkToSync(sourceUser, subject1_mr1);

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject1), Collections.singletonList(subject1_mr1));
    }

    @Test
    @HardDependency("testOkToSyncSuccessfulSync")
    public void testOkToSyncRemark() {
        assertTrue(markExperimentNotOkToSync(sourceUser, subject1_mr1).contains("this session has already been synced"));
        final String remarkOkResponse = markExperimentOkToSync(sourceUser, subject1_mr1);
        assertTrue(remarkOkResponse.contains("this session has already been synced"));
        assertTrue(remarkOkResponse.contains("It will not be scheduled to be resynced"));

        markExperimentOkToSync(sourceUser, subject1_mr2);

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject1), Collections.singletonList(subject1_mr2));
    }

    @Test
    @HardDependency("testOkToSyncRemark")
    public void testOkToSyncManualOverwrite() {
        final ImagingSession destinationSession = findDestinationSessionShortcut(subject1_mr1);
        final Scan scan0 = destinationSession.getScans().get(0);
        final Scan scan1 = destinationSession.getScans().get(1);
        final Resource scan1Dicom = destinationXnatClient.findResource(scan1.getScanResources(), "DICOM");

        addSessionNotes(destinationXnatClient, destinationUser, "XSYNC TESTING NOTE", destinationSession);
        destinationXnatClient.deleteScan(destinationUser, scan0);
        destinationXnatClient.deleteResource(destinationUser, scan1Dicom);

        manualSync(sourceUser, subject1_mr1);

        final XSyncResult result = waitForSyncComplete(sourceUser, DEFAULT_WAIT, sourceProject);
        assertEquals(1, result.getCompletedSubjects().size());
        assertEquals(1, result.getCompletedExperiments().size());

        final XSyncHistory history = readSyncHistory(sourceUser, sourceProject, result);
        //assertEquals(OverallStatus.COMPLETE_NOT_VERIFIED, history.getSyncStatus());
        assertEquals(SyncStatus.SYNCED_AND_VERIFIED, historyFor(history, subject1_mr1).getSyncStatus());
        assertEquals(1, history.getExperimentHistories().size());

        final ImagingSession overwrittenSession = findDestinationSessionShortcut(subject1_mr1);
        final List<Scan> overwrittenScans = overwrittenSession.getScans();
        final Scan overwrittenScan1 = destinationXnatClient.findScan(overwrittenSession, scan1.getId());
        final Resource overwrittenScan1Dicom = destinationXnatClient.findResource(overwrittenScan1.getScanResources(), "DICOM");

        assertEquals(null, overwrittenSession.getNotes());
        assertEquals(destinationSession.getScans(), overwrittenScans);
        simpleResourceCheck(scan1Dicom, overwrittenScan1Dicom);
    }


}
