package org.nrg.xnat.xsync.tests;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.io.DicomInputStream;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.file.FileIO;
import org.nrg.xnat.pogo.AnonScript;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.extensions.anon.AnonScriptFileExtension;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.xsync.BaseXSyncTest;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@SuppressWarnings("FieldCanBeLocal")
@TestRequires(data = TestData.XSYNC_DATA)
public class XSyncAnonymizationTests extends BaseXSyncTest {

    private final XSyncConfig config = new XSyncConfig().anonymize(true);

    @BeforeClass
    public void setupConfig() {
        final AnonScript script = new AnonScript();
        config.anonScript(new AnonScript().extension(new AnonScriptFileExtension(script, FileIO.getDataFile("scanner_sample_anon.das"))));
        baseXSyncSetup(config);
    }

    @Test
    public void testSyncAnon() throws IOException {
        final int totalNumFiles = 1604;
        final Subject subject = new Subject().label("SPP_0x960c08");
        final ImagingSession session = new MRSession("SPP_0x960c08_MR1").subject(subject);
        final int scanCount = 8;
        final List<Subject> subjects = Collections.singletonList(subject);
        final List<ImagingSession> sessions = Collections.singletonList(session);
        int processedFileCount = 0;

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        session.scans(restDriver.readScans(sourceUser, sourceProject, subject, session));

        launchProjectSync(sourceUser, sourceProject);
        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, subjects, sessions);

        final ImagingSession syncedSession = findDestinationSessionShortcut(session);
        assertEquals(scanCount, syncedSession.getScans().size());

        for (Scan syncedScan : syncedSession.getScans()) {
            final Scan presyncScan = restDriver.findScan(session, syncedScan.getId());
            final Resource presyncDicom = dicomFrom(presyncScan.getScanResources());
            final Resource syncedDicom = dicomFrom(syncedScan.getScanResources());

            assertEquals(presyncDicom.getFileCount(), syncedDicom.getFileCount());

            final List<String> dicomFileUris = destinationXnatClient.interfaceFor(destinationUser).jsonQuery().get(destinationXnatClient.formatXnatUrl(syncedDicom.resourceUrl(), "resources", syncedDicom.getFolder(), "files")).
                    then().assertThat().statusCode(200).and().extract().response().jsonPath().getList("ResultSet.Result.URI");

            for (String uri : dicomFileUris) {
                final File dicomFile = destinationXnatClient.saveBinaryResponseToFile(destinationXnatClient.interfaceFor(destinationUser).queryBase().get(destinationXnatClient.formatXnatUrl(uri)));
                final DicomInputStream dicomStream = new DicomInputStream(dicomFile);
                final Attributes dicomElements = dicomStream.readDataset(-1, -1);
                assertEquals("Scanner values modified by XSync Anon Script (Station Name)", dicomElements.getString(Tag.StationName));
                assertEquals("Scanner values modified by XSync Anon Script (Manufacturer)", dicomElements.getString(Tag.Manufacturer));
                assertEquals("Scanner values modified by XSync Anon Script (Manufacturer's Model Name)", dicomElements.getString(Tag.ManufacturerModelName));
                dicomStream.close();
                processedFileCount++;
                if (processedFileCount % 100 == 0) LOGGER.info(String.format("%d DICOM files asserted to be anonymized correctly so far (out of %d).", processedFileCount, totalNumFiles));
            }
        }

        assertEquals(totalNumFiles, processedFileCount);
    }


}
