package org.nrg.xnat.xsync.tests;

import com.google.common.collect.Sets;
import org.nrg.testing.annotations.TestRequires;
import org.nrg.testing.enums.TestData;
import org.nrg.testing.file.FileIO;
import org.nrg.xnat.pogo.Subject;
import org.nrg.xnat.pogo.experiments.ImagingSession;
import org.nrg.xnat.pogo.experiments.Scan;
import org.nrg.xnat.pogo.experiments.sessions.MRSession;
import org.nrg.xnat.pogo.extensions.SimpleResourceFileExtension;
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension;
import org.nrg.xnat.pogo.resources.Resource;
import org.nrg.xnat.pogo.resources.ResourceFile;
import org.nrg.xnat.pogo.resources.ScanResource;
import org.nrg.xnat.pogo.resources.SubjectAssessorResource;
import org.nrg.xnat.xsync.BaseXSyncTest;
import org.nrg.xnat.xsync.XSyncConfigFileExtension;
import org.nrg.xnat.xsync.config.XSyncConfig;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.*;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

@TestRequires(data = TestData.XSYNC_DATA)
public class AdvancedSessionSyncFunctionsTests extends BaseXSyncTest {

    private final XSyncConfig config = new XSyncConfig();
    private final File sessionZip = getXSyncData("SPP_0x960c08_MR1");
    private final String t1wScanType = "T1w";
    private final String t2wScanType = "T2w";
    private final String filteredQuality = "unusable";
    private final String allowedSessionResource = "RESOURCE1";
    private final String disallowedSessionResource = "RESOURCE2";
    private final String disallowedSessionResource2 = "RESOURCE3";
    private final File binaryFile = FileIO.getDataFile("binary.png");
    private final List<String> syncedScanTypes = Arrays.asList(t1wScanType, t2wScanType);

    @BeforeClass
    public void setupConfig() {
        new XSyncConfigFileExtension(config, FileIO.getDataFile("advanced_session_sync_config.json"), sourceProject, destinationXnat.getXnatUrl(), destinationProject);
        baseXSyncSetup(config);
    }

    @Test
    public void testScanTypeSyncing() {
        final Subject subject = new Subject(sourceProject, "scan_type_subject");
        final MRSession session = new MRSession(sourceProject, subject, "scan_type_MR");
        session.extension(new SessionImportExtension(restDriver.mainInterface(), session, sessionZip));
        final List<MRSession> sessions = Collections.singletonList(session);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        final List<Scan> syncedScans = restDriver.filterScansByType(restDriver.readScans(sourceUser, sourceProject, subject, session), syncedScanTypes);

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject), sessions);

        assertEquals(syncedScans, findDestinationSessionShortcut(session).getScans());
    }

    @Test
    public void testScanTypeFilterSyncing() {
        final Subject subject = new Subject(sourceProject, "scan_filter_subject");
        final MRSession session = new MRSession(sourceProject, subject, "scan_filter_MR");
        session.extension(new SessionImportExtension(restDriver.mainInterface(), session, sessionZip));
        final List<MRSession> sessions = Collections.singletonList(session);

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        final List<Scan> sourceScans = restDriver.readScans(sourceUser, sourceProject, subject, session);
        final List<Scan> t1Scans = restDriver.filterScansByType(sourceScans, t1wScanType);
        final List<Scan> t2Scans = restDriver.filterScansByType(sourceScans, t2wScanType);

        final Scan t1 = t1Scans.get(0).quality(filteredQuality);
        final Scan t2 = t2Scans.get(0).quality(filteredQuality);
        restDriver.updateScan(sourceUser, t1);
        restDriver.updateScan(sourceUser, t2);

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject), sessions);

        final List<Scan> actualScans = findDestinationSessionShortcut(session).getScans();
        final List<Scan> expectedScans = new ArrayList<>();
        expectedScans.addAll(t1Scans);
        expectedScans.addAll(t2Scans);
        expectedScans.remove(t1);
        expectedScans.remove(t2);

        assertEquals(new HashSet<>(expectedScans), new HashSet<>(actualScans));
    }

    @Test
    public void testScanResourceFilterSyncing() {
        final Subject subject = new Subject(sourceProject, "scan_resource_filter_subject");
        final MRSession session = new MRSession(sourceProject, subject, "scan_resource_filter_MR");
        session.extension(new SessionImportExtension(restDriver.mainInterface(), session, sessionZip));
        final List<MRSession> sessions = Collections.singletonList(session);
        final String other = "other";

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        final List<Scan> scansToBeSynced = restDriver.filterScansByType(restDriver.readScans(sourceUser, sourceProject, subject, session), syncedScanTypes);
        final Scan scan1 = scansToBeSynced.get(0);
        final Scan scan2 = scansToBeSynced.get(1);
        final Scan scan3 = scansToBeSynced.get(2);

        final Resource scan1Nifti = new ScanResource(sourceProject, subject, session, scan1, NIFTI).addResourceFile(new ResourceFile().name(binaryFile.getName()).extension(new SimpleResourceFileExtension(binaryFile)));
        final Resource scan2Nifti = new ScanResource(sourceProject, subject, session, scan2, NIFTI).addResourceFile(new ResourceFile().name("zippy.zip").unzip(true).extension(new SimpleResourceFileExtension(getXSyncData("SPP_0x960c08_MR1"))));
        final Resource scan2Other = new ScanResource(sourceProject, subject, session, scan2, other).addResourceFile(new ResourceFile().name(binaryFile.getName()).extension(new SimpleResourceFileExtension(binaryFile)));
        final Resource scan3Other = new ScanResource(sourceProject, subject, session, scan3, other).addResourceFile(new ResourceFile().name(binaryFile.getName()).extension(new SimpleResourceFileExtension(binaryFile)));

        restDriver.uploadResources(sourceUser, Arrays.asList(scan1Nifti, scan2Nifti, scan2Other, scan3Other));

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject), sessions);

        final ImagingSession syncedSession = findDestinationSession(destinationUser, destinationProject, subject.getLabel(), session.getLabel());
        final List<Scan> syncedScans = syncedSession.getScans();
        final Scan syncedScan1 = destinationXnatClient.findScan(syncedSession, scan1.getId());
        final Scan syncedScan2 = destinationXnatClient.findScan(syncedSession, scan2.getId());
        final Scan syncedScan3 = destinationXnatClient.findScan(syncedSession, scan3.getId());

        assertEquals(scansToBeSynced, syncedScans);

        final Resource scan1Dicom = dicomFrom(scan1.getScanResources());
        final Resource scan2Dicom = dicomFrom(scan2.getScanResources());
        final Resource scan3Dicom = dicomFrom(scan3.getScanResources());

        assertResourcesMatch(Sets.newHashSet(scan1Dicom, scan1Nifti), syncedScan1.getScanResources());
        assertResourcesMatch(Sets.newHashSet(scan2Dicom, scan2Nifti), syncedScan2.getScanResources());
        assertResourcesMatch(Collections.singleton(scan3Dicom),       syncedScan3.getScanResources());

        simpleResourceCheck(scan1Dicom, dicomFrom(syncedScan1.getScanResources()));
        simpleResourceCheck(scan2Dicom, dicomFrom(syncedScan2.getScanResources()));
        simpleResourceCheck(scan3Dicom, dicomFrom(syncedScan3.getScanResources()));
        simpleResourceCheck(scan2Nifti, niftiFrom(syncedScan2.getScanResources()));
        destinationXnatClient.validateResource(destinationUser, scan1Nifti.project(destinationProject).subject(syncedSession.getSubject()).subjectAssessor(syncedSession)); // check binary
    }

    @Test
    public void testSessionResourceFilterSyncing() {
        final Subject subject = new Subject(sourceProject, "session_resource_filter_subject");
        final MRSession session = new MRSession(sourceProject, subject, "session_resource_filter_MR");
        session.extension(new SessionImportExtension(restDriver.mainInterface(), session, sessionZip));
        final List<MRSession> sessions = Collections.singletonList(session);
        final Resource allowedResource = new SubjectAssessorResource(sourceProject, subject, session, allowedSessionResource);
        final Resource disallowedResource = new SubjectAssessorResource(sourceProject, subject, session, disallowedSessionResource);
        final Resource disallowedResource2 = new SubjectAssessorResource(sourceProject, subject, session, disallowedSessionResource2);
        for (Resource resource : new Resource[]{allowedResource, disallowedResource, disallowedResource2}) {
            resource.addResourceFile(new ResourceFile().name(binaryFile.getName()).extension(new SimpleResourceFileExtension(binaryFile)));
        }

        uploadAndLogSessions(sourceUser, sourceProject, sessions);

        launchProjectSync(sourceUser, sourceProject);

        assertAllSyncedAndVerified(sourceUser, DEFAULT_WAIT, sourceProject, Collections.singletonList(subject), sessions);

        final ImagingSession syncedSession = findDestinationSessionShortcut(session);
        final List<Resource> syncedResources = syncedSession.getResources();

        assertResourcesMatch(Collections.singleton(allowedResource), syncedResources);
        destinationXnatClient.validateResource(destinationUser, allowedResource.project(destinationProject).subject(syncedSession.getSubject()).subjectAssessor(syncedSession));
    }

    private void assertResourcesMatch(Collection<Resource> expectedResources, List<Resource> actualResources) {
        assertEquals(expectedResources.size(), actualResources.size());
        for (Resource resource : expectedResources) {
            assertNotNull(restDriver.findResource(actualResources, resource.getFolder()));
        }
    }


}
